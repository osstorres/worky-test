# Test worky
## https://worky-test.herokuapp.com/

superuser : admin-worky, password: worky

disclaimer : Debug mode is ON and Several sensitive configurations are exposed in files and debugs just for this test, 

For this project I used django, vue and heroku as server, the integration of vue with django was through the same django server and the dependencies were built to be able to integrate it into the django statics within the dist folder that generates npm, the database is in postgresql.

Local configuration 

```
python3 -m venv .env
source .env/bin/activate
pip install -r requirements.txt
python3 manage.py runserver

```
# Backend

For the backend I used django-rest-framework to manage the api, a user application was created with a user model that inherited from abstractuser, a manager was created to manage the creation and validation of whether or not a user exists, serializers were used to handle validations and custom responses and errors.

### Endpoints
- [https://worky-test.herokuapp.com/api/v1.0/](https://worky-test.herokuapp.com/api/v1.0/) BASE URL
-  [https://worky-test.herokuapp.com/api/v1.0/signin/](https://worky-test.herokuapp.com/api/v1.0/signin/) Register user (authentication NOT required)
- [https://worky-test.herokuapp.com/api/v1.0/users/](https://worky-test.herokuapp.com/api/v1.0/users/) Get, Post model users
- [https://worky-test.herokuapp.com/api/v1.0/token/obtain_token/](https://worky-test.herokuapp.com/api/v1.0/token/obtain_token/) JWT token, obtain token with user and password, for test the lifetime is 10 days.
- [https://worky-test.herokuapp.com/api/v1.0/token/refresh_token/](https://worky-test.herokuapp.com/api/v1.0/token/refresh_token/) JWT token, refresh token with user and password, for test the lifetime refresh is 12 days.

Api with django-rest-framework was built to consume the user model information within the users application.

Integration with vue

```
STATICFILES_DIRS = [  
  os.path.join(BASE_DIR, 'frontend/dist/static/')  
]
```



# BD
For the database postgresql was used inside the heroku server, for this test the credentials are inside the file **secrets.json**

# Frontend

The frontend is integrated in the frontend folder built with CLI Vue, the components were created : createuser, login, logout, sigin and users, in turn vuex was used to manage the authentication with JWT of the backend, for this test there are several debugs to display information on the console and the tokens are managed in localstorage (Do not do this in production). In order to get the code into production, we built (npm run built) the dependencies into the dist folder for django to manage the static files.

```
npm run build
```


![frontend](/media/frontend.png)

- createuser.vue : Component for adding users in the administration panel (authentication required)  
- login :  Component for logging in with a user (authentication NOT required)
- logout : Logout component (clears localstorage token)
- signin : Component to register in the application (Authentication not required)
- users : Component to search for user by id and navigate to add a user (Authentication required)
- store : Component for managing authentication with vuex (JWT)



# CI/CD

To deploy the application to the server, two stages were created in the gitlab pipes, the **build stage** to build the docker image and push it to the repository, and the **deploy stage** that uses a script to execute the heroku deploy command.

# Architecture


![architecture](/media/architecture.png)
