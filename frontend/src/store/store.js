import Vuex from 'vuex'
import Vue from 'vue'
import * as types from './mutation-types'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import router from '../router'
import Swal from "sweetalert2";

const API_URL = 'https://worky-test.herokuapp.com/api/v1.0/'
const LOGIN_URL = API_URL + 'token/obtain_token/'

Vue.use(Vuex)

const state = {
  user: {},
  token: null
}

const mutations = {
  [types.LOGIN]: (state, payload) => {
    state.token = payload.token
    state.user = payload.user
    router.push(payload.redirect)
  },
  [types.LOGOUT]: (state, payload) => {
    state.token = null
    state.user = {}
    router.push(payload.redirect)
  },
}

const actions = {
  [types.LOGIN] ({ commit }, payload) {
    axios.post(LOGIN_URL, payload.credential)
    .then(response => {
      if (response.data) {
        var mutationPayload = {}
        mutationPayload.token = response.data.access
        mutationPayload.user = JSON.parse(atob(response.data.access.split('.')[1]))
        mutationPayload.user.authenticated = true
        console.log('mutationPayload just for worky test, remove in future!!!', mutationPayload)
        mutationPayload.redirect = payload.redirect
        commit(types.LOGIN, mutationPayload)
      }
    })
    .catch(e => {
        Swal.fire({
  icon: 'error',
  title: 'Oops... ',
  text: 'Try again',
})
    })
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  plugins: [
    createPersistedState()
  ]
})

export default store
