import Vue from 'vue'
import Router from 'vue-router'
import main from "@/components/main"
import createuser from "@/components/createuser"
import users from "@/components/users"
import login from "@/components/login";
import logout from "@/components/logout";
import store from "@/store/store";
import signin from '@/components/signin'
Vue.use(Router);


const routes= [
    { path: '/', redirect: { name: 'login' }},
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
    path: '/signin',
    name: 'Signin',
    component: signin
  },
    {
      path: '/logout',
      name: 'logout',
      component: logout
    },
    {
      path: '/users/create/',
      name: 'createuser',
      component: createuser,
       meta: {
      requireAuth: true
    },
    },

    {
      path: '/users/',
      name: 'users',
      component: users,
      meta: {
      requireAuth: true
    },
    },

  ];


const router = new Router({
  routes,
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {
    if (store.state.token) {
      next()
    } else {
      next({
        path: '/login',
        query: {redirect: to.fullPath}
      })
    }
  } else {
    next()
  }
});


export default router
