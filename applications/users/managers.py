from django.db import models
from django.contrib.auth.models import BaseUserManager


# Manager for user abstract model

class UserManager(BaseUserManager, models.Manager):

    def _create_user(self, user, email, password, is_staff, is_superuser, is_active, **extra_fields):
        user = self.model(
            user=user,
            email=email,
            is_staff=is_staff,
            is_superuser=is_superuser,
            is_active=is_active,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_user(self, user, email, password=None, **extra_fields):
        return self._create_user(user, email, password, False, False, True, **extra_fields)

    def create_superuser(self, user, email, password=None, **extra_fields):
        return self._create_user(user, email, password, True, True, True, **extra_fields)

    def user_exists(self, user):
        return self.model.objects.filter(user=user)