from django.db import models

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from .managers import UserManager

# Model to manage users


class User(AbstractBaseUser, PermissionsMixin):

    user = models.CharField(max_length=50, unique=True)
    email = models.EmailField()
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    USERNAME_FIELD = 'user'
    REQUIRED_FIELDS = ['email', ]

    objects = UserManager()

