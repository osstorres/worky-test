from django.shortcuts import render
from rest_framework import viewsets, status, permissions
from rest_framework.response import Response
from rest_framework.exceptions import APIException

from .serializers import (
    UserSerializer,
    CustomException
)
from .models import User


def index(request):
    return render(request, 'index.html')


class UserViewSet(viewsets.ModelViewSet, APIException):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    http_method_names = ['get', 'post', ]

    def create(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)
        user_exists = serializer.is_valid_user(serializer.initial_data['user'])
        if user_exists:
            # message to frontend that this user already exists
            return Response({"message": f"User already exists with this info id : {user_exists.pk},"
                                        f" email: {user_exists.email},"
                                        f" first name:  {user_exists.first_name},"
                                        f" last name: {user_exists.last_name} "},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, *args, **kwargs):

        """
        Validation get method for requirement rampage
        """
        if self.request.method == 'GET':
            try:
                instance = self.get_object()
                serializer = self.get_serializer(instance)
                return Response(serializer.data)
            except Exception:
                raise CustomException("User does not exist", status_code=status.HTTP_204_NO_CONTENT)


class SignInViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    authentication_classes = []
    serializer_class = UserSerializer
    queryset = User.objects.all()
    http_method_names = ['post', ]
