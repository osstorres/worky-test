from . import views
from rest_framework import routers
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views

app_name = 'users_app'
router = routers.SimpleRouter()
router.register('users', views.UserViewSet)
router.register('signin', views.SignInViewSet)

urlpatterns = [
    # main url for api rest
    path('api/v1.0/', include(router.urls)),
    # urls for jwt
    path('api/v1.0/token/obtain_token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1.0/token/refresh_token/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    # urls for templates frontend
    path('users/', views.index, name='users'),
    path('users/create/', views.index, name='users_create'),
    path('login/', views.index, name='login'),
    path('', views.index, name='index')
]
