from rest_framework import serializers, status
from .models import User


class UserSerializer(serializers.ModelSerializer):
    """
    Custom camelCase for requirement ultra kill
    """
    firstName = serializers.CharField(source="first_name")
    lastName = serializers.CharField(source="last_name")

    class Meta:
        model = User
        fields = (
            'id',
            'user',
            'firstName',
            'lastName',
            'email',
            'password',
        )

    def is_valid_user(self, user):
        user_exists = User.objects.user_exists(user=self.initial_data['user'])
        return user_exists.get() if user_exists else None

    def create(self, validated_data):

        user = User.objects.create_user(
            user=validated_data['user'],
            email=validated_data['email'],
            password=validated_data['password'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        return user


class CustomException(serializers.ValidationError):
    """
    raises API exceptions custom for requirement status code 204 when user does not exist Rampage
    """
    status_code = status.HTTP_400_BAD_REQUEST
    default_code = 'error'

    def __init__(self, detail, status_code=None):
        self.detail = detail
        if status_code is not None:
            self.status_code = status_code
